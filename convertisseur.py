from moviepy.editor import *

 #git 
#Converti des video en gif code simple 
def convert_video_to_gif(input_path_file,output_path_file,start_time_video=0,end_time_video = None,resize_file=None):
 with VideoFileClip(input_path_file) as video:
    if end_time_video: 
        video = video.subclip(start_time_video,end_time_video)
    else:
        video = video.subclip(start_time_video)
    
    if(resize_file):
        video.write_gif(output_path_file)
        
 # call function        path file video to convert                                            create file  start video timer   end video timer  resize file    
convert_video_to_gif("", "play.gif" , start_time_video=6 ,end_time_video=9 , resize_file=(500,500))